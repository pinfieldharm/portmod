# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
"""
Module that interacts with the portmod config file

Files are stored both in the portmod local directory and in the profile directory tree,
with the user's config file overriding and extending defaults set by the profile
"""

from typing import Dict
import configparser
import os
from functools import lru_cache
from .globals import OPENMW_CONFIG, PORTMOD_CONFIG
from .repo.profiles import profile_parents
from .repo.flags import collapse_flags

__COLLAPSE_KEYS = {"USE", "ACCEPT_LICENSE"}
__EXTEND_KEYS = {"ACCEPT_KEYWORDS", "PORTMOD_MIRRORS"}
__OVERRIDE_KEYS = {"ARCH", "OPENMW_CONFIG", "TEXTURE_SIZE"}


def read_config(path: str) -> Dict:
    """
    Reads a config file and converts the relevant fields into a dictionary
    """
    with open(path, "r") as file:
        config = file.read()

    parser = configparser.ConfigParser()
    if "[general]" not in config:
        parser.read_string("[general]\n" + config)
    else:
        parser.read_string(config)
    config_dict = {}
    config_dict["USE"] = set(parser["general"].get("USE", "").split())
    config_dict["ARCH"] = parser["general"].get("ARCH")
    config_dict["OPENMW_CONFIG"] = parser["general"].get("OPENMW_CONFIG")
    if config_dict["OPENMW_CONFIG"]:
        config_dict["OPENMW_CONFIG"] = os.path.expanduser(config_dict["OPENMW_CONFIG"])
    config_dict["ACCEPT_KEYWORDS"] = (
        parser["general"].get("ACCEPT_KEYWORDS", "").split()
    )
    config_dict["ACCEPT_LICENSE"] = parser["general"].get("ACCEPT_LICENSE", "").split()
    config_dict["TEXTURE_SIZE"] = parser["general"].get("TEXTURE_SIZE")
    config_dict["PORTMOD_MIRRORS"] = (
        parser["general"].get("PORTMOD_MIRRORS", "").split()
    )
    return config_dict


def merge_configs(old_config: Dict, new_config: Dict) -> Dict:
    """
    Merges two configs together, combining their values appropriately
    """
    merged = old_config

    for key in __COLLAPSE_KEYS:
        merged[key] = collapse_flags(merged.get(key, set()), new_config.get(key, set()))

    for key in __OVERRIDE_KEYS:
        if key in new_config and new_config[key]:
            merged[key] = new_config.get(key)

    for key in __EXTEND_KEYS:
        if key in merged:
            merged[key].extend(new_config.get(key, []))
        else:
            merged[key] = new_config.get(key, [])

    for key in old_config.keys() | new_config.keys():
        if key not in __COLLAPSE_KEYS | __OVERRIDE_KEYS | __EXTEND_KEYS:
            if key in new_config:
                merged[key] = new_config[key]
            else:
                merged[key] = old_config[key]

    return merged


@lru_cache(maxsize=None)
def get_config() -> Dict:
    """
    Parses the user's configuration, overriding defaults from their profile
    """
    total_config = {
        # Default cannot be set in profile due to the value depending on platform
        "OPENMW_CONFIG": OPENMW_CONFIG
    }
    for parent in profile_parents():
        path = os.path.join(parent, "defaults.cfg")
        if os.path.exists(path):
            config = read_config(path)
            total_config = merge_configs(total_config, config)

    if os.path.exists(PORTMOD_CONFIG):
        config = read_config(PORTMOD_CONFIG)
        total_config = merge_configs(total_config, config)

    # Apply environment variables onto config
    for key in __EXTEND_KEYS:
        if key in os.environ and key in total_config:
            total_config[key].extend(os.environ[key])
        elif key in os.environ:
            total_config[key] = os.environ[key]

    for key in __OVERRIDE_KEYS:
        if key in os.environ:
            total_config[key] = os.environ[key]

    for key in __COLLAPSE_KEYS:
        if key in os.environ:
            total_config[key] = collapse_flags(
                total_config.get(key, set()), os.environ[key]
            )

    for key in total_config:
        if key not in __COLLAPSE_KEYS | __OVERRIDE_KEYS | __EXTEND_KEYS:
            if isinstance(total_config[key], str):
                os.environ[key] = total_config[key]
    return total_config


def config_to_string(config: Dict) -> str:
    """
    Prints the given dictionary config as a string

    The resulting string is suitable for reading by read_config
    """
    lines = []
    for key in sorted(config):
        if isinstance(config[key], set) or isinstance(config[key], list):
            lines.append("{} = {}".format(key, " ".join(sorted(config[key]))))
        else:
            lines.append("{} = {}".format(key, config[key]))
    return "\n".join(lines)
