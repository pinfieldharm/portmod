# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys
import tempfile
import git
from appdirs import user_data_dir, user_cache_dir, user_config_dir

DEBUG = False

APP_NAME = "portmod"
APP_AUTHOR = "portmod"

HOME = os.path.expanduser("~")
OPENMW_CONFIG_DIR = user_config_dir("openmw", "openmw")

# Manually override OPENMW_CONFIG_DIR paths on win32 and darwin
#   according to https://openmw.readthedocs.io/en/stable/reference/modding/paths.html
if sys.platform == "darwin":
    OPENMW_CONFIG_DIR = os.path.join(HOME, "/Library/Preferences/openmw")
elif sys.platform == "win32":
    import ctypes.wintypes

    CSIDL_PERSONAL = 5  # My Documents
    SHGFP_TYPE_CURRENT = 0  # Get current, not default value

    __BUF = ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(
        None, CSIDL_PERSONAL, None, SHGFP_TYPE_CURRENT, __BUF
    )
    OPENMW_CONFIG_DIR = os.path.join(__BUF.value, "my games", "openmw")

OPENMW_CONFIG = os.path.join(OPENMW_CONFIG_DIR, "openmw.cfg")

TMP_DIR = os.path.join(tempfile.gettempdir(), "portmod")

PORTMOD_CONFIG_DIR = user_config_dir(APP_NAME, APP_AUTHOR)
SET_DIR = os.path.join(PORTMOD_CONFIG_DIR, "sets")
PORTMOD_CONFIG = os.path.join(PORTMOD_CONFIG_DIR, "portmod.cfg")

PORTMOD_LOCAL_DIR = user_data_dir(APP_NAME, APP_AUTHOR)
MOD_DIR = os.path.join(PORTMOD_LOCAL_DIR, "mods")
CACHE_DIR = user_cache_dir(APP_NAME, APP_AUTHOR)
DOWNLOAD_DIR = os.path.join(CACHE_DIR, "downloads")

INSTALLED_DB = os.path.join(PORTMOD_LOCAL_DIR, "db")
PORTMOD_MIRRORS_DEFAULT = "https://gitlab.com/portmod/mirror/raw/master/"

ALLOW_LOAD_ERROR = True

# Ensure that INSTALLED_DB exists
if not os.path.exists(INSTALLED_DB):
    # Initialize as git repo
    os.makedirs(INSTALLED_DB)
    gitrepo = git.Repo.init(INSTALLED_DB)
