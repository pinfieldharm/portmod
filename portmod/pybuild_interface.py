# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from typing import AbstractSet, List, Set, Tuple

import os
from portmod.repo.atom import Atom
from portmod.globals import DOWNLOAD_DIR


class Source:
    """Class used for storing information about download files"""

    def __init__(self, url: str, name: str):
        self.url = url
        self.name = name
        self.shasum = ""
        self.size = -1
        self.path = os.path.join(DOWNLOAD_DIR, name)

    def manifest(self, shasum: str, size: int):
        """Updates source to include values in manifest"""
        self.shasum = shasum
        self.size = size

    def __repr__(self):
        return self.url


# Class used for typing pybuilds, allowing more flexibility with
# the implementations. Implementations of this class (e.g. Pybuild1)
# should derive it, but build file Mod classes should derive one of
# the implementations. This should be used as the type for any function that
# handles Pybuild objects.
#
# This provides a mechanism for modifying the Pybuild format, as we can
# make changes to this interface, and update the implementations to conform
# to it while keeping their file structure the same, performing conversions
# of the data inside the init function.
class Pybuild:
    """Interface describing the Pybuild Type"""

    ATOM: Atom
    RDEPEND: str
    DEPEND: str
    SRC_URI: str
    MN: str
    C: str
    REQUIRED_USE: str
    RESTRICT: str
    IUSE_EFFECTIVE: str

    def get_default_sources(self) -> List[Source]:
        """
        Returns a list of sources that are enabled
        with the current use configuration
        """

    def get_sources(
        self,
        uselist: AbstractSet[str],
        masklist: AbstractSet[str],
        matchnone=False,
        matchall=False,
    ) -> List[Source]:
        """
        Returns a list of sources that are enabled using the given configuration
        """

    def get_use(self) -> Tuple[Set[str], Set[str]]:
        """Returns the use flag configuration for the mod"""


class InstalledPybuild(Pybuild):
    """Interface describing the type of installed Pybuilds"""

    INSTALLED_USE: Set[str]
