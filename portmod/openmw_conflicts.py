#!/usr/bin/python

# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import shutil
import subprocess
from portmod.config import find_config, read_config


def main():
    config = read_config()

    mod_dirs = [directory for (index, directory) in find_config(config, "data", "*")]

    args = ["dcv"]
    args.extend(mod_dirs)

    if shutil.which("dcv"):
        subprocess.Popen(args).wait()
    else:
        print('Error: Could not find "dcv"')


if __name__ == "__main__":
    main()
