import inspect
import os
from pathlib import Path
from portmod.repo.atom import Atom

"""
Importing values from this module fills them with information about the file
from which they were imported
This module should be removed from sys.modules prior to importing to ensure that
the cached version is not used instead.
"""

__FILENAME = None
for i in inspect.stack(0):
    if i.filename.endswith(".pybuild"):
        __FILENAME = i.filename
assert __FILENAME is not None
CATEGORY = Path(__FILENAME).resolve().parent.parent.name
__ATOM = Atom(
    "{}/{}".format(CATEGORY, os.path.basename(__FILENAME)[: -len(".pybuild")])
)

M = __ATOM.M
MF = __ATOM.MF
MN = __ATOM.MN
MV = __ATOM.MV
MR = __ATOM.MR
MVR = __ATOM.MV
if __ATOM.MR is not None:
    MVR += "-" + __ATOM.MR
