# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import subprocess
import shlex
from portmod.log import warn  # noqa  # pylint: disable=unused-import
from portmod.globals import DOWNLOAD_DIR  # noqa  # pylint: disable=unused-import
from portmod.util import patch_dir  # noqa  # pylint: disable=unused-import
from .pybuild import (  # noqa  # pylint: disable=unused-import
    Pybuild1,
    InstallDir,
    File,
    apply_patch,
)


def execute(command):
    """Executes command"""
    subprocess.check_call(shlex.split(command))
