# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import fnmatch
import shutil
import re
from operator import itemgetter
from ..globals import OPENMW_CONFIG, OPENMW_CONFIG_DIR


# Returns config file as a list of strings (one string per line)
def read_config():
    config = {"data": [], "content": [], "fallback-override": []}
    with open(OPENMW_CONFIG, mode="r") as config_file:
        for line in config_file.read().splitlines():
            if len(line) == 0:
                continue
            (var, val) = line.split("=")
            if config.get(var) is None:
                config[var] = []
            config[var].append(val)
    return config


# Replaces config file with the given
def write_config(new_config):
    # We make a back up of the config file
    shutil.copy(OPENMW_CONFIG, OPENMW_CONFIG_DIR + "openmw.cfg.bak")

    print("Saving changes made to MW config")

    # Save config categories in reverse alphabetical order
    with open(OPENMW_CONFIG, mode="w") as config:
        for (key, values) in sorted(new_config.items())[::-1]:
            for line in values:
                print("{}={}".format(key, line), file=config)


def add_config(config, prefix, name, index=-1):
    if check_config(config, prefix, name) != -1:
        return

    if prefix not in config:
        config[prefix] = []

    if index == -1:
        config[prefix].append(name)
    else:
        config[prefix].insert(index, name)

    print('Added "{}" to config'.format("{}={}".format(prefix, name)))


def check_config(config, prefix, name):
    """
    Checks if config contains a line matching the parameters.
    Supports globbing in the prefix and name
    """
    for line in config.get(prefix, []):
        if fnmatch.fnmatch(line, name) or fnmatch.fnmatch(line, '"{}"'.format(name)):
            return config[prefix].index(line)
    return -1


def check_config_subdirs(config, prefix, name):
    index = check_config(config, prefix, name)
    if index == -1:
        return check_config(config, prefix, "{}/*".format(name))
    return index


def find_config(config, prefix, name):
    """
    Returns index-value pairs for lines in config that match the given prefix and name
    """
    lines = []
    for (index, line) in enumerate(config[prefix]):
        if fnmatch.fnmatch(line, name) or fnmatch.fnmatch(line, '"{}"'.format(name)):
            match = re.match('^("(.*)"|(.*))', line)
            newline = match.group(2) if match.group(2) else match.group(1)
            lines.append((index, newline))
    return lines


def find_config_subdirs(config, prefix, name):
    return find_config(config, prefix, name) + find_config(
        config, prefix, "{}/*".format(name)
    )


def Diff(li1, li2):
    li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2]
    return li_dif


def remove_config(config, prefix, name):
    """
    Removes lines from the config matching the parameters.
    Supports globbing in the prefix and name
    """
    to_remove = find_config(config, prefix, name)
    # delete in reverse order to preserve indexes
    for (index, line) in sorted(to_remove, key=itemgetter(0), reverse=True):
        del config[prefix][index]
        print('Removed "{}" from config'.format(line))
    return to_remove


def remove_config_subdirs(config, prefix, name):
    return remove_config(config, prefix, name) + remove_config(
        config, prefix, "{}/*".format(name)
    )
