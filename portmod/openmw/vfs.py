import os
from .config import read_config
from ..util import ci_exists


def find_file(name: str) -> str:
    """
    Locates the path of a file within the OpenMW virtual file system
    """
    for directory in read_config()["data"][::-1]:
        directory = directory.lstrip('"').rstrip('"')
        path = os.path.join(directory, name)
        path = ci_exists(path)
        if path:
            return path

    raise FileNotFoundError(name)
