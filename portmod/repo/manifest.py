# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import glob
from enum import Enum
import os
from portmod.repo.download import (
    download_source,
    get_hash,
    get_download,
    clobber_spaces,
)
from portmod.log import err
from .loader import load_mod
from .atom import Atom


class ManifestFile:
    def __init__(self, file):
        self.entries = {}
        self.FILE = file
        if os.path.exists(file):
            with open(file, "r") as manifest:
                for line in manifest.readlines():
                    filetype, name, size, shasum = line.split()
                    self.entries[name] = Manifest(
                        name, FileType[filetype], shasum, size
                    )

    def add_entry(self, entry):
        if entry is None:
            raise Exception("Adding None to manifest")
        ours = self.entries.get(entry.NAME)
        if ours is None or not ours.to_string() == entry.to_string():
            self.entries[entry.NAME] = entry

    def write(self):
        with open(self.FILE, "w") as manifest:
            lines = [entry.to_string() for entry in self.entries.values()]
            lines.sort()
            for line in lines:
                print(line, file=manifest)

    def get(self, name):
        return self.entries.get(name)


class FileType(Enum):
    PYBUILD = "PYBUILD"
    DIST = "DIST"
    AUX = "AUX"
    MISC = "MISC"


class Manifest:
    def __init__(self, name, filetype, shasum, size):
        self.NAME = name
        if type(filetype) is not FileType:
            raise Exception(
                "filetype {} of manifest entry must be a FileType".format(filetype)
            )
        self.FILE_TYPE = filetype
        self.SHASUM = shasum
        self.SIZE = int(size)

    def to_string(self):
        return "{} {} {} {}".format(
            self.FILE_TYPE.name, self.NAME, self.SIZE, self.SHASUM
        )


# Atomatically download mod DIST files (if not already in cache)
# and create a manifest file
def create_manifest(mod):
    clobber_spaces()
    pybuild_file = mod.FILE
    manifest = ManifestFile(manifest_path(pybuild_file))
    manifest.entries = {}

    directory = os.path.dirname(pybuild_file)

    for file in glob.glob(os.path.join(directory, "*.pybuild")):
        name, ext = os.path.splitext(os.path.basename(file))
        atom = Atom(mod.CATEGORY + "/" + name)
        thismod = load_mod(atom)

        # Add ebuild to manifest
        manifest.add_entry(
            Manifest(
                os.path.basename(file),
                FileType.PYBUILD,
                get_hash(file),
                os.path.getsize(file),
            )
        )

        sources = thismod[0].get_sources([], [], matchall=True)

        # Add sources to manifest
        for source in sources:
            filename = download_source(mod, source, False)
            if filename is None:
                err("Unable to get shasum for unavailable file " + source.name)
                continue

            shasum = get_hash(filename)
            size = os.path.getsize(filename)

            manifest.add_entry(Manifest(source.name, FileType.DIST, shasum, size))

    metadata_file = os.path.join(os.path.dirname(pybuild_file), "metadata.yaml")
    # Add other files to manifest
    if os.path.exists(metadata_file):
        shasum = get_hash(metadata_file)
        manifest.add_entry(
            Manifest(
                "metadata.yaml", FileType.MISC, shasum, os.path.getsize(metadata_file)
            )
        )

    aux_dir = os.path.join(os.path.dirname(pybuild_file), "files")
    if os.path.exists(aux_dir):
        for file in os.listdir(aux_dir):
            path = os.path.join(aux_dir, file)
            shasum = get_hash(path)
            manifest.add_entry(
                Manifest(file, FileType.AUX, shasum, os.path.getsize(path))
            )

    # Write changes to manifest
    manifest.write()


# Verify that files match the manifest for a given mod.
def verify_manifest(mod):
    # TODO: Implement
    pass


def manifest_path(file):
    return os.path.join(os.path.dirname(file), "Manifest")


# Loads the manifest for the given file, i.e. the Manifest file in the same directory
#    and turns it into a map of filenames to (shasum, size) pairs
def get_manifest(file):
    m_path = manifest_path(file)

    return ManifestFile(m_path)


def get_total_download_size(mods):
    download_bytes = 0
    for mod in mods:
        manifest_file = get_manifest(mod.FILE)
        for manifest in manifest_file.entries.values():
            if manifest.FILE_TYPE == FileType.DIST:
                download_bytes += manifest.SIZE

    return "{:.3f} MiB".format(download_bytes / 1024 / 1024)


def get_download_size(mods):
    clobber_spaces()
    download_bytes = 0
    for mod in mods:
        manifest_file = get_manifest(mod.FILE)
        sources = mod.get_default_sources()
        for manifest in manifest_file.entries.values():
            source = next(
                (source for source in sources if source.name == manifest.NAME), None
            )
            if (
                manifest.FILE_TYPE == FileType.DIST
                and source is not None
                and get_download(manifest.NAME, manifest.SHASUM) is None
            ):
                download_bytes += manifest.SIZE

    return "{:.3f} MiB".format(download_bytes / 1024 / 1024)
