# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from typing import List, Set
from ..globals import PORTMOD_CONFIG_DIR
from .atom import Atom
from .list import read_list


def profile_parents() -> List[str]:
    """
    Produces the paths of all the parent directories for the selected profile, in order
    """
    profilepath = os.path.join(PORTMOD_CONFIG_DIR, "profile")
    if not os.path.exists(profilepath) or not os.path.islink(profilepath):
        raise Exception(
            f"{profilepath} does not exist.\n"
            "Please choose a profile before attempting to install mods"
        )
    first = os.readlink(profilepath)

    def get_parents(directory: str) -> List[str]:
        parentfile = os.path.join(directory, "parent")
        parents = []
        if os.path.exists(parentfile):
            for parent in read_list(parentfile):
                parentpath = os.path.normpath(os.path.join(directory, parent))
                parents.extend(get_parents(parentpath))
                parents.append(parentpath)

        return parents

    parents = [first]
    parents.extend(get_parents(first))
    return parents


def get_system() -> Set[Atom]:
    """Calculates the system set using the user's currently selected profile"""
    system = set()
    for parent in profile_parents():
        mods = os.path.join(parent, "mods")
        if os.path.exists(mods):
            system |= {
                Atom(mod.lstrip("*")) for mod in read_list(mods) if mod.startswith("*")
            }

    return system
