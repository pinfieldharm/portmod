# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import sys
import importlib.machinery
import glob
import os
import traceback
from portmod.log import warn
from portmod.repo.atom import atom_sat
from portmod.globals import INSTALLED_DB
from portmod.repos import REPOS
from portmod.repo.metadata import get_categories
import portmod.globals
from .atom import Atom


# We store a cache of mods so that they are only loaded once
# when doing dependency resolution.
# Stores key-value pairs of the form (filename, Mod Object)
__mods = {}


def load_file(file):
    # Ensure that we never use the cached version of modinfo
    if "portmod.pybuild.modinfo" in sys.modules:
        del sys.modules["portmod.pybuild.modinfo"]

    filename, _ = os.path.splitext(os.path.basename(file))
    loader = importlib.machinery.SourceFileLoader(filename, file)
    try:
        mod = loader.load_module().Mod()
    except Exception as e:
        warn(e)
        if portmod.globals.DEBUG:
            traceback.print_exc()
        warn('Could not load pybuild "{}"'.format(filename))
        if portmod.globals.ALLOW_LOAD_ERROR:
            return None
        raise e
    mod.FILE = os.path.abspath(file)
    mod.INSTALLED = False
    return mod


def load_all_installed():
    """
    Returns every single installed mod in the form of a map from their simple mod name
    to their mod object
    """
    mods = {}
    repo = INSTALLED_DB

    for path in glob.glob(os.path.join(repo, "*/*")):
        mod = __load_installed_mod(path)
        if mod is not None:
            if mods.get(mod.MN) is None:
                mods[mod.MN] = [mod]
            else:
                mods[mod.MN].append(mod)
    return mods


def __load_installed_mod(path):
    if os.path.exists(path):
        files = glob.glob(os.path.join(path, "*.pybuild"))
        if len(files) > 1:
            atom = Atom(
                os.path.basename(os.path.dirname(files[0]))
                + "/"
                + os.path.basename(files[0].rstrip(".pybuild"))
            )
            raise Exception('Multiple versions of mod "{}" installed!'.format(atom))
        elif len(files) == 0:
            return None

        for file in files:
            if __mods.get(file, False):
                return __mods[file]
            else:
                mod = load_file(file)
                if mod is None:
                    continue

                mod.INSTALLED = True
                with open(os.path.join(path, "REPO"), "r") as repo_file:
                    mod.REPO = repo_file.readlines()[0].rstrip()
                    mod.DISPLAY_ATOM = mod.ATOM + "::" + mod.REPO
                with open(os.path.join(path, "USE"), "r") as use_file:
                    mod.INSTALLED_USE = set(use_file.readlines()[0].split())

                __mods[file] = mod
                return mod
    return None


# Loads mods from the installed database
def load_installed_mod(atom):
    repo = INSTALLED_DB

    path = None
    if atom.C:
        path = os.path.join(repo, atom.C, atom.MN)
    else:
        for dirname in glob.glob(os.path.join(repo, "*")):
            path = os.path.join(repo, dirname, atom.MN)
            if os.path.exists(path):
                break

    if path is not None:
        mod = __load_installed_mod(path)
    else:
        return None

    if mod is None or not atom_sat(mod.ATOM, atom):
        return None
    else:
        return mod


def load_mod(atom):
    """
    Loads all mods matching the given atom
    There may be multiple versions in different repos,
    as well versions with different version or release numbers
    """
    mods = []

    path = None
    for repo in REPOS:
        if not os.path.exists(repo.location):
            warn(
                "Repository {} does not exist at configured location {}".format(
                    repo.name, repo.location
                )
            )
            print(
                'You might need to run "mwmerge --sync" if this is a remote repository'
            )

        if atom.C:
            path = os.path.join(repo.location, atom.C, atom.MN)
        else:
            for category in get_categories(repo.location):
                path = os.path.join(repo.location, category, atom.MN)
                if os.path.exists(path):
                    break

        if path is not None and os.path.exists(path):
            for file in glob.glob(os.path.join(path, "*.pybuild")):
                if __mods.get(file, False):
                    mod = __mods[file]
                else:
                    mod = load_file(file)
                    if mod is None:
                        continue

                    mod.INSTALLED = False
                    __mods[file] = mod
                if atom_sat(mod.ATOM, atom):
                    mods.append(mod)
    return mods


def load_all():
    mods = []
    for repo in REPOS:
        for category in get_categories(repo.location):
            for file in glob.glob(
                os.path.join(repo.location, category, "*", "*.pybuild")
            ):
                if __mods.get(file, False):
                    mod = __mods[file]
                else:
                    mod = load_file(file)
                    if mod is None:
                        continue

                    __mods[file] = mod
                mods.append(mod)
    return mods
