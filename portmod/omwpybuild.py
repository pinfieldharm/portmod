#!/usr/bin/python

# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import sys
import argparse
from os import path as osp


def main():
    if osp.isfile(
        osp.join(
            osp.dirname(osp.dirname(osp.realpath(__file__))), ".portmod_not_installed"
        )
    ):
        sys.path.insert(0, osp.dirname(osp.dirname(osp.realpath(__file__))))

    import traceback
    import portmod.globals
    from portmod.repo.loader import load_file
    from portmod.repo.download import download_mod
    from portmod.log import err
    from portmod.mod import install_mod, remove_mod
    from portmod.main import pybuild_validate, pybuild_manifest

    parser = argparse.ArgumentParser(
        description="Command line interface to interact with pybuilds"
    )
    parser.add_argument("pybuild_file", metavar="<pybuild file>")
    parser.add_argument(
        "command",
        metavar="<command>",
        nargs="+",
        choices=[
            "manifest",
            "fetch",
            "unpack",
            "prepare",
            "install",
            "qmerge",
            "merge",
            "unmerge",
            "validate",
        ],
    )
    parser.add_argument("--debug", help="Enables debug traces", action="store_true")
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()

    if args.debug:
        portmod.globals.DEBUG = True
    try:
        for command in args.command:
            if command == "manifest":
                pybuild_manifest(args.pybuild_file)
            elif command == "fetch":
                mod = load_file(args.pybuild_file)
                sources = download_mod(mod)
                if not sources and not mod.NO_DOWNLOAD:
                    print("Unable to fetch mod archives. Aborting.")
                    exit(1)
            elif command == "merge":
                mod = load_file(args.pybuild_file)
                install_mod(mod)
            elif command == "unmerge":
                remove_mod(mod)
            elif command == "validate":
                pybuild_validate(args.pybuild_file)
    except Exception as e:
        if args.debug:
            traceback.print_exc()
        err("{}".format(e))


if __name__ == "__main__":
    main()
