#!/usr/bin/env python

# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3


from setuptools import setup

setup(
    name="portmod",
    version="1.0",
    author="Portmod Authors",
    description="https://gitlab.com/portmod/portmod",
    download_url="A CLI tool to manage mods for OpenMW",
    license="GPLv3",
    url="https://gitlab.com/portmod/portmod",
    packages=["portmod", "portmod.repo", "portmod.pybuild", "portmod.openmw"],
    entry_points=(
        {
            "console_scripts": [
                "inquisitor = portmod.inquisitor:main",
                "omwmerge = portmod.omwmerge:main",
                "omwmirror = portmod.mirror:mirror",
                "omwuse = portmod.omwuse:main",
                "openmw-conflicts = portmod.openmw_conflicts:main",
                "pybuild = portmod.omwpybuild:main",
                "omwselect = portmod.select:main",
            ]
        }
    ),
    install_requires=[
        "patool",
        "colorama",
        "appdirs",
        "black",
        "GitPython",
        "PyYAML",
        "progressbar2",
        'pywin32; platform_system == "Windows"',
    ],
)
