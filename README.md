# Portmod
A cli tool to manage mods for OpenMW

## Python Dependencies
Can be installed through pip, or via your package manager. When installing using the pip commands given later in this file these will be installed automatically.

- patool: http://wummel.github.io/patool/
    - Archive manager
    - Note that patool relies on helper programs to handle may file types, you will need these programs installed and in your PATH to be able to extract files of types not supported natively by patool (built-in types include TAR, ZIP, BZIP2 and GZIP). Archive types encountered by Portmod vary, but you probably want to have tools that can handle at least RAR, 7z and XZ archives.
- colorama
- appdirs
- black
- GitPython
- PyYAML
- progressbar2

## Other Dependencies
### omwcmd
A native cli tool written in Rust that makes use of esplugin to quickly parse elder scrolls plugin files. Used for getting the masters of plugins.

https://gitlab.com/bmwinger/omwcmd

Binaries can be found on the tags page: https://gitlab.com/portmod/omwcmd/-/tags

Otherwise, you can compile it yourself using Rust: https://www.rust-lang.org/tools/install

The binary should be installed somewhere in your system PATH so that Portmod can find it.

## Installing on Linux

To install into `/usr/bin` (requires root), run

    git clone https://gitlab.com/portmod/portmod
    sudo pip3 install /path/to/portmod

Or, directly from Gitlab:

    sudo pip3 install git+https://gitlab.com/portmod/portmod

To install into `~/.local/bin`, run

    git clone https://gitlab.com/portmod/portmod
    pip3 install --user --upgrade /path/to/portmod

Or, directly from Gitlab:

    pip3 install --user --upgrade git+https://gitlab.com/portmod/portmod

## Installing on Windows

Note that Windows support is currently untested, however there is no reason to suggest that it will not work on Windows.

It is recommended that you use pip to install with `pip3 install git+https://gitlab.com/portmod/portmod`

In addition to the above dependencies, you will need to install Git, if it has not already been installed. The python dependencies can be installed with pip, which comes with Python. You may also want to install 7-zip, which covers most archive types not supported natively by patool, including the commonly encountered types (see note on patool in dependencies section above).

https://git-scm.com/download/win

## Setup

### Synchronize the Repo
The first thing you should do after installing is to synchronize [the mod repository](https://gitlab.com/portmod/openmw-mods). This can be done through portmod via `omwmerge --sync`.

### Select a Profile

With the repository synchronized, you need to choose a profile before you can begin installing mods. The profile provides default settings that are tailored for your game setup.

Run `omwselect profile list` to see a list of available profiles. See [Profiles](https://gitlab.com/portmod/portmod/wikis/profiles) for details about the different profile options. `omwselect profile set NUM` can be used to select the profile you want.

Once you've chosen a profile, you should run a global update to install any mods required by your profile. You can do this using `omwmerge --update --deep --newuse @world` (or `omwmerge -uDN @world`).

## Basic Usage

Mods can be installed by passing the relevant atoms as command line arguments. E.g.:
`omwmerge omwllf`

You can search for mods using the `--search`/`-s` (searches name only) and `--searchdesc`/`-S` (searches name and description) options.

Specific versions of mods can be installed by including the version number: `omwmerge abandoned-flat-2.0`

Specified mods will be automatically be downloaded, configured and installed.

The `-c` flag will remove the specified mods and all mods that depend on, or are dependencies of, the specified mods.

The `-C` flag will remove the specified mods, ignoring dependencies

## Notes

- Make sure the openmw-launcher is not running. If you add mods when the openmw-launcher is running the mods will be removed from the config file when the openmw-launcher is closed.
- When installing mods, relevant plugins are enabled automatically. There is no need to enable plugins using openmw-launcher.
- In general, manual configuration is not recommended when using Portmod as it may lead to undesired behaviour, notably, the potential loss of your changes when running updates. If there is a configuration step that a pybuild omits you have the following options:
    1. Report the issue and wait for a fix
    2. Fix the pybuild yourself:
        - Create a local repository and add it to repos.cfg in the config directory
        - Copy and modify the offending pybuild.
        - Once you've added and tested the fix, please submit the changes to the repository so that others can benefit from the change.

This project is not affiliated with OpenMW
